# -*- coding: utf-8 -*-

from datetime import datetime
from dateutil.relativedelta import relativedelta

from odoo import models, fields, api
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DATE_FORMAT
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT as DATETIME_FORMAT
from odoo.exceptions import ValidationError


class JAGOCetakSlipGajiWizard(models.TransientModel):
    _name = 'cetak.slip.gaji.report.wizard'
    _description = 'Modul untuk custom wizard'

    month = fields.Selection([('1', 'Januari'), ('2', 'Februari'), ('3', 'Maret'), ('4', 'April'), ('5', 'Mei'), ('6', 'Juni'), ('7', 'Juli'), (
        '8', 'Agustus'), ('9', 'September'), ('10', 'Oktober'), ('11', 'November'), ('12', 'Desember')], string='Bulan', default='%s' % (fields.Date.today().month))
    year = fields.Char(string='Tahun', default=fields.Date.today().year)

    def get_cetak_slip_gaji(self):
        from_date = fields.datetime(int(self.year),int(self.month),1).strftime("%Y-%m-%d")
        to_date = (fields.datetime(int(self.year),int(self.month),1) + relativedelta(months = 1)).strftime("%Y-%m-%d")
        payslip_data = self.env['hr.payslip'].search([('date_from','>=',from_date),('date_to','<',to_date)]).ids
        data = {
            'model': self._name,
            'ids': self.ids,
            'form': {
                'month': self.month,
                'year': self.year,
                'object': payslip_data,
            },
        }
        
        if payslip_data:
            return self.env.ref('jago_hr_employee.report_cetak_slip_gaji').report_action(self, data=data)
        else:
            raise ValidationError("Data Tidak Ada") 
        
class JAGOCetakSlipGajiReport(models.AbstractModel):
    """
        Abstract Model specially for report template.
        _name = Use prefix `report.` along with `module_name.report_name`
    """
    _name = 'report.jago_hr_employee.report_cetak_slip_gaji_view'
    _description = 'Modul untuk custom report Daftar Tagihan Piutang'

    @api.model
    def _get_report_values(self, docids, data=None):
        month = data['form']['month']
        year = data['form']['year']
        payslip_datas = data['form']['object']
        payslip_data = self.env['hr.payslip'].search([('id','in', payslip_datas)])
        return {
            'doc_ids': data['ids'],
            'doc_model': data['model'],
            'object': payslip_data,
            'month': month,
            'year': year,
        }