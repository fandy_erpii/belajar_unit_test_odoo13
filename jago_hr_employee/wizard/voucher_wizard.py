"""Report Kode Finance"""
# from odoo.tools.misc import xlsxwriter
from odoo import models, fields, api, _
from datetime import datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from io import BytesIO
import calendar
import base64
import io
from odoo.exceptions import UserError
try:
    from odoo.tools.misc import xlsxwriter
except ImportError:
    import xlsxwriter


class KodeFinanceWizard(models.TransientModel):
    _name = "kode.finance.wizard"
    _description = "Report Kode Finance"

    @api.model
    def _get_year(self):
        now = int(datetime.now().year)
        return now

    month = fields.Selection([('01', 'Januari'), ('02', 'Februari'), ('03', 'Maret'), ('04', 'April'), ('05', 'Mei'), ('06', 'Juni'),
                              ('07', 'Juli'), ('08', 'Agustus'), ('09', 'September'), ('10', 'Oktober'), ('11', 'November'), ('12', 'Desember')],
                             string='Bulan')
    year = fields.Char(string='Tahun', default=_get_year)
    kode_finance_ids = fields.Many2many('kode.finance', string="Kode Finance")
    state_x = fields.Selection(
        (('choose', 'choose'), ('get', 'get')), default='choose')
    data_x = fields.Binary('File', readonly=True)
    name = fields.Char('Filename', readonly=True)
    check_all = fields.Boolean(string='Semua', default=False)

    @api.onchange('month', 'year')
    def onchange_month(self):
        if self.month and self.year:
            lastday = calendar.monthrange(int(self.year), int(self.month))[1]
            self.date_from = '%s-%s-01' % (int(self.year), self.month)
            self.date_to = '%s-%s-%s' % (int(self.year), self.month, lastday)

    def convert_format_date(self, date):
        date = datetime.strptime(date, DF)
        date = date.strftime(DF)
        return date

    def month_name(self, month_number):
        return calendar.month_name[month_number]

    def month_number(self, month_number):
        return calendar.month_name

    def generate_excel(self):
        self.ensure_one()
        bz = BytesIO()
        workbook = xlsxwriter.Workbook(bz)
        if self.year and self.month:
            filename = 'Laporan Voucher ('+str(self.year)+').xls'
        else:
            filename = 'Laporan Voucher.xls'

        if self.kode_finance_ids:
            kd_ids = self.kode_finance_ids
        else:
            kd_ids = self.env['kode.finance'].search([])

        query = """
            select
            kf.id,
            he.id,
            hp.id,
            kf.id as kode_id,
            kf.name as kode_name,
            kf.finance_code as kode_finance,
            kf.finance_type as tipe,
            count(he.id) as emp_count,
            hsr.id as rule_id,
            hsr.code as rule_code,
            hp.date_to as hp_date,
            sum(hpl.total) as slip_line_total
            from kode_finance kf
            join hr_employee he on kf.id =he.lokasi_kerja_id
            join hr_payslip hp on hp.employee_id=he.id
            left join hr_payslip_line hpl on hpl.slip_id = hp.id
            left join hr_salary_rule hsr on hsr.id = hpl.salary_rule_id

        """

        params = ()
        if self.check_all:
            if not self.year:
                query += """ """
            else:
                year = self.year
                query += "where extract(year from hp.date_to) = %s"
                params += (year,)
            if self.month:
                month = self.month
                query += " AND extract(month from hp.date_to) = %s"
                params += (month,)
        elif not self.check_all and not self.kode_finance_ids:
            raise UserError(
                _('Centang Field Semua atau Pilih Kode Finance !'))
        else:
            query += """
            where kf.id in %s
            """
            params = (tuple(self.kode_finance_ids.ids),)
            if self.month:
                month = self.month
                query += " AND extract(month from hp.date_to) = %s"
                params += (month,)
            if self.year:
                year = self.year
                query += " AND extract(year from hp.date_to) = %s"
                params += (year,)

        query += """
            group by kf.id, kf.finance_code, hsr.id, hsr.code, he.id, hp.id
            order by hp.date_to asc
        """

        self.env.cr.execute(query, params)
        datas = self.env.cr.dictfetchall()
        if not datas:
            raise UserError(_('No Data'))

        title_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 16,
             'valign': 'vcenter', 'align': 'center', 'text_wrap': True,
             'font_color': 'blue'})
        #################################################################################
        normal_style = workbook.add_format(
            {'valign': 'vcenter', 'text_wrap': True,
             'font_size': 11, 'font_name': 'Arial'})
        #################################################################################
        normal_left = workbook.add_format(
            {'valign': 'bottom', 'align': 'left'})
        normal_left.set_text_wrap()
        normal_left.set_font_name('Arial')
        normal_left.set_font_size('10')
        normal_left.set_border()
        #################################################################################
        normal_right = workbook.add_format(
            {'valign': 'bottom', 'align': 'right'})
        normal_right.set_text_wrap()
        normal_right.set_font_name('Arial')
        normal_right.set_font_size('10')
        normal_right.set_border()
        #################################################################################
        normal_left1 = workbook.add_format(
            {'valign': 'bottom', 'align': 'left'})
        normal_left1.set_text_wrap()
        normal_left1.set_font_name('Arial')
        normal_left1.set_font_size('10')
        #################################################################################
        normal_left_bold = workbook.add_format(
            {'bold': 1, 'valign': 'bottom', 'align': 'left'})
        normal_left_bold.set_text_wrap()
        normal_left_bold.set_font_name('Arial')
        normal_left_bold.set_font_size('10')
        #################################################################################
        normal_bold_border = workbook.add_format(
            {'bold': 0, 'valign': 'vcenter', 'align': 'center'})
        normal_bold_border.set_text_wrap()
        normal_bold_border.set_font_name('Arial')
        normal_bold_border.set_font_size('10')
        normal_bold_border.set_border()

        worksheet = workbook.add_worksheet("report")
        worksheet.set_column('A:A', 13)
        worksheet.set_column('B:B', 9)
        worksheet.set_column('C:C', 13)
        worksheet.set_column('D:D', 17)
        worksheet.set_column('E:AB', 13)
        worksheet.set_column('AC:AC', 20)

        row = 1
        result = {}
        list_kode = []
        list_month = []
        for dt in datas:
            kode = dt.get('kode_id')
            month_int = fields.Date.from_string(
                dt.get('hp_date')).strftime('%m')
            year = fields.Date.from_string(dt.get('hp_date')).strftime('%Y')
            list_kode.append(kode)
            list_month.append(month_int)
            rule = dt.get('rule_code')
            jkk_jkm = 0
            gp_a = 0
            transfer = 0
            total = 0

            # for line in dt:
            #     if month_int == '01':
            #         month_str = 'Januari'
            #     elif month_int == '02':
            #         month_str = 'Februari'
            #     elif month_int == '03':
            #         month_str = 'Maret'
            #     elif month_int == '04':
            #         month_str = 'April'
            #     elif month_int == '05':
            #         month_str = 'Mei'
            #     elif month_int == '06':
            #         month_str = 'Juni'
            #     elif month_int == '07':
            #         month_str = 'Juli'
            #     elif month_int == '08':
            #         month_str = 'Agustus'
            #     elif month_int == '09':
            #         month_str = 'September'
            #     elif month_int == '10':
            #         month_str = 'Oktober'
            #     elif month_int == '11':
            #         month_str = 'November'
            #     elif month_int == '12':
            #         month_str = 'Desember'

            # key = "%s_%s" % (kode, month_int)
            key = "%s" % (month_int)
            if key not in result:
                result[key] = { '%s' % (kode) : {
                    # result [kode] = {
                    'kode_finance': dt.get('kode_finance'),
                    'name': dt.get('kode_name'),
                    'month': month_int,
                    'tipe': dt.get('tipe'),
                    'year': year,
                    'nilai': dt.get('slip_line_total'),
                    'rule_code': rule,
                    'emp_count': dt.get('emp_count'),
                    'bpjs_amount': rule == 'BPJKS' and dt.get('slip_line_total') or 0,
                    'jkk_amount': rule == 'JKKP' and dt.get('slip_line_total') or 0,
                    'jkm_amount': rule == 'JKMP 'and dt.get('slip_line_total') or 0,
                    'gp_amount': rule == 'BASIC' and dt.get('slip_line_total') or 0,
                    'thadir_amount': rule == 'TJHDR' and dt.get('slip_line_total') or 0,
                    'jabt_amount': rule == 'TJBTN' and dt.get('slip_line_total') or 0,
                    'mskj_amount': rule == 'TMSKJ' and dt.get('slip_line_total') or 0,
                    'trns_amount': rule == 'TTRANS' and dt.get('slip_line_total') or 0,
                    'bras_amount': rule == 'TBRS' and dt.get('slip_line_total') or 0,
                    'tddik_amount': rule == 'TDDIK' and dt.get('slip_line_total') or 0,
                    'thr_amount': rule == 'THR' and dt.get('slip_line_total') or 0,
                    # 'gra_amount': rule == '' and dt.get('slip_line_total') or 0,
                    'bns_amount': rule == 'BNS' and dt.get('slip_line_total') or 0,
                    'lembur_amount': rule == 'LMBR' and dt.get('slip_line_total') or 0,
                    'upd_amount': rule == 'TUPD' and dt.get('slip_line_total') or 0,
                    # 'premi_amount': rule == 'THR' and dt.get('slip_line_total') or 0,
                    'rapel_amount': rule == 'RPL' and dt.get('slip_line_total') or 0,
                    'pot_amount': rule == 'PL' and dt.get('slip_line_total') or 0,
                    'bruto_amount': rule == 'GROSS_PAJAK' and dt.get('slip_line_total') or 0,
                    'astek_amount': rule == 'ASTEK-K' and dt.get('slip_line_total') or 0,
                    'jpk_amount': rule == 'JPBK' and dt.get('slip_line_total') or 0,
                    'pph_amount': rule == 'PPH21' and dt.get('slip_line_total') or 0,
                    'bpjskes_amount': rule == 'BPJSK' and dt.get('slip_line_total') or 0,
                    # 'total_amount': rule == 'GROSS' and dt.get('slip_line_total') or 0,
                    'lain_amount': rule == 'PL' and dt.get('slip_line_total') or 0,
                    # 'bpjskes_amount': rule == 'BPJSK' and dt.get('slip_line_total') or 0,
                    'bltn_amount': rule == 'ROUND' and dt.get('slip_line_total') or 0,
                    'total_amount': rule == 'GATOT' and dt.get('slip_line_total') or 0,
                    'transfer_amount': rule == 'NET' and dt.get('slip_line_total') or 0,
                    'proyek_amount': rule == 'PRYK' and dt.get('slip_line_total') or 0,
                }}
            else:
                if '%s' % (kode) not in result[key].keys():
                    result[key].update({ '%s' % (kode) : {
                        'kode_finance': dt.get('kode_finance'),
                        'name': dt.get('kode_name'),
                        'month': month_int,
                        'tipe': dt.get('tipe'),
                        'year': year,
                        'nilai': dt.get('slip_line_total'),
                        'rule_code': rule,
                        'emp_count': dt.get('emp_count'),
                        'bpjs_amount': dt.get('slip_line_total') if rule == 'BPJKS' else 0,
                        'jkk_amount': dt.get('slip_line_total') if rule == 'JKKP' else 0,
                        'jkm_amount': dt.get('slip_line_total') if rule == 'JKMP 'else 0,
                        'gp_amount': dt.get('slip_line_total') if rule == 'BASIC' else 0,
                        'thadir_amount': dt.get('slip_line_total') if rule == 'TJHDR' else 0,
                        'jabt_amount': dt.get('slip_line_total') if rule == 'TJBTN' else 0,
                        'mskj_amount': dt.get('slip_line_total') if rule == 'TMSKJ' else 0,
                        'trns_amount': dt.get('slip_line_total') if rule == 'TTRANS' else 0,
                        'bras_amount': dt.get('slip_line_total') if rule == 'TBRS' else 0,
                        'tddik_amount': dt.get('slip_line_total') if rule == 'TDDIK' else 0,
                        'thr_amount': dt.get('slip_line_total') if rule == 'THR' else 0,
                        'bns_amount': dt.get('slip_line_total') if rule == 'BNS' else 0,
                        'lembur_amount': dt.get('slip_line_total') if rule == 'LMBR' else 0,
                        'upd_amount': dt.get('slip_line_total') if rule == 'TUPD' else 0,
                        'rapel_amount': dt.get('slip_line_total') if rule == 'RPL' else 0,
                        'pot_amount': dt.get('slip_line_total') if rule == 'PL' else 0,
                        'bruto_amount': dt.get('slip_line_total') if rule == 'GROSS_PAJAK' else 0,
                        'astek_amount': dt.get('slip_line_total') if rule == 'ASTEK-K' else 0,
                        'jpk_amount': dt.get('slip_line_total') if rule == 'JPBK' else 0,
                        'pph_amount': dt.get('slip_line_total') if rule == 'PPH21' else 0,
                        'bpjskes_amount': dt.get('slip_line_total') if rule == 'BPJSK' else 0,
                        # 'total_amount': dt.get('slip_line_total') if rule == 'GROSS' else  0,
                        'lain_amount': dt.get('slip_line_total') if rule == 'PL' else 0,
                        # 'bpjskes_amount': dt.get('slip_line_total') if rule == 'BPJSK' else  0,
                        'bltn_amount': dt.get('slip_line_total') if rule == 'ROUND' else 0,
                        'total_amount': dt.get('slip_line_total') if rule == 'GATOT' else 0,
                        'transfer_amount': dt.get('slip_line_total') if rule == 'NET' else 0,
                        'proyek_amount': dt.get('slip_line_total') if rule == 'PRYK' else 0,
                        }})
                else:
                    vals = result[key]['%s' % (kode)]
                    vals['bpjs_amount'] += rule == 'BPJKS' and dt.get(
                        'slip_line_total') or 0
                    vals['jkk_amount'] += rule == 'JKKP' and dt.get(
                        'slip_line_total') or 0
                    vals['jkm_amount'] += rule == 'JKMP 'and dt.get(
                        'slip_line_total') or 0
                    jkk_jkm = vals['jkk_amount'] + vals['jkm_amount']
                    vals['gp_amount'] += rule == 'BASIC' and dt.get(
                        'slip_line_total') or 0
                    vals['thadir_amount'] += rule == 'TJHDR' and dt.get(
                        'slip_line_total') or 0
                    vals['jabt_amount'] += rule == 'TJBTN' and dt.get(
                        'slip_line_total') or 0
                    vals['mskj_amount'] += rule == 'TMSKJ' and dt.get(
                        'slip_line_total') or 0
                    vals['trns_amount'] += rule == 'TTRANS' and dt.get(
                        'slip_line_total') or 0
                    vals['bras_amount'] += rule == 'TBRS' and dt.get(
                        'slip_line_total') or 0
                    vals['tddik_amount'] += rule == 'TDDIK' and dt.get(
                        'slip_line_total') or 0
                    vals['thr_amount'] += rule == 'THR' and dt.get(
                        'slip_line_total') or 0
                    vals['bns_amount'] += rule == 'BNS' and dt.get(
                        'slip_line_total') or 0
                    vals['lembur_amount'] += rule == 'LMBR' and dt.get(
                        'slip_line_total') or 0
                    vals['upd_amount'] += rule == 'TUPD' and dt.get(
                        'slip_line_total') or 0
                    vals['rapel_amount'] += rule == 'RPL' and dt.get(
                        'slip_line_total') or 0
                    vals['pot_amount'] += rule == 'PL' and dt.get(
                        'slip_line_total') or 0
                    vals['bruto_amount'] += rule == 'GROSS_PAJAK' and dt.get(
                        'slip_line_total') or 0
                    vals['astek_amount'] += rule == 'ASTEK-K' and dt.get(
                        'slip_line_total') or 0
                    vals['jpk_amount'] += rule == 'JPBK' and dt.get(
                        'slip_line_total') or 0
                    vals['pph_amount'] += rule == 'PPH21' and dt.get(
                        'slip_line_total') or 0
                    vals['bpjskes_amount'] += rule == 'BPJSK' and dt.get(
                        'slip_line_total') or 0
                    vals['total_amount'] += rule == 'GATOT' and dt.get(
                        'slip_line_total') or 0
                    vals['lain_amount'] += rule == 'PL' and dt.get(
                        'slip_line_total') or 0
                    vals['bltn_amount'] += rule == 'ROUND' and dt.get(
                        'slip_line_total') or 0
                    vals['transfer_amount'] += rule == 'NET' and dt.get(
                        'slip_line_total') or 0
                    vals['proyek_amount'] += rule == 'PRYK' and dt.get(
                        'slip_line_total') or 0

        print(result)
        for line in result:
            worksheet.set_row(row, 35)
            if line == '01':
                month_str = 'Januari'
            elif line == '02':
                month_str = 'Februari'
            elif line == '03':
                month_str = 'Maret'
            elif line == '04':
                month_str = 'April'
            elif line == '05':
                month_str = 'Mei'
            elif line == '06':
                month_str = 'Juni'
            elif line == '07':
                month_str = 'Juli'
            elif line == '08':
                month_str = 'Agustus'
            elif line == '09':
                month_str = 'September'
            elif line == '10':
                month_str = 'Oktober'
            elif line == '11':
                month_str = 'November'
            elif line == '12':
                month_str = 'Desember'

            worksheet.write(
                row, 0, self.env.user.company_id.name, normal_bold_border)
            worksheet.write(row, 1, month_str, normal_bold_border)
            worksheet.write(row, 2,  dt.get('kode_name'), normal_bold_border)
            row += 1

            worksheet.merge_range(
                'A1:AD1', '', title_style)
            worksheet.merge_range(
                'D2:AB2', '', title_style)
            worksheet.merge_range(
                'A3:C3', '', normal_bold_border)
            worksheet.merge_range(
                'M3:O3', 'PENDAPATAN TIDAK TETAP', normal_bold_border)
            worksheet.merge_range(
                'R3:T3', 'PENAMBAH/PENGURANG SBLM PAJAK', normal_bold_border)
            worksheet.merge_range(
                'W3:Y3', 'DIBAYAR K BUKAN OBYEK PPH', normal_bold_border)
            worksheet.merge_range(
                'Z3:AB3', 'PENAMBAH/PENGURANG SETELAH PAJAK', normal_bold_border)

            worksheet.write(row, 3, 'DIBAYAR P OBJEK PPH', normal_bold_border)
            worksheet.write(row, 4, '', normal_bold_border)
            worksheet.write(row, 5, 'GP + TJ TETAP', normal_bold_border)
            worksheet.write(row, 6, ' ', normal_bold_border)
            worksheet.write(row, 7, ' ', normal_bold_border)
            worksheet.write(row, 8, ' ', normal_bold_border)
            worksheet.write(row, 9, 'BONUS', normal_bold_border)
            worksheet.write(row, 10, ' ', normal_bold_border)
            worksheet.write(row, 11, 'KEHADIRAN', normal_bold_border)
            worksheet.write(row, 12, '', normal_bold_border)
            worksheet.write(row, 13, 'PENDAPATAN TIDAK TETAP',
                            normal_bold_border)
            worksheet.write(row, 14, '', normal_bold_border)
            worksheet.write(row, 15, '', normal_bold_border)
            worksheet.write(row, 16, '', normal_bold_border)
            worksheet.write(row, 17, '', normal_bold_border)
            worksheet.write(
                row, 18, 'PENAMBAH/PENGURANG SBLM PAJAK', normal_bold_border)
            worksheet.write(row, 19, '', normal_bold_border)
            worksheet.write(row, 20, 'PPH', normal_bold_border)
            worksheet.write(row, 21, '', normal_bold_border)
            worksheet.write(row, 22, '', normal_bold_border)
            worksheet.write(
                row, 23, 'DIBAYAR K BUKAN OBYEK PPH', normal_bold_border)
            worksheet.write(row, 24, '', normal_bold_border)
            worksheet.write(row, 25, '', normal_bold_border)
            worksheet.write(
                row, 26, 'PENAMBAH/PENGURANG SETELAH PAJAK', normal_bold_border)
            worksheet.write(row, 27, '', normal_bold_border)
            worksheet.write(row, 28, 'DITERIMA K', normal_bold_border)
            row += 1

            worksheet.write(row, 0, '0.4', normal_bold_border)
            worksheet.write(row, 1, '0.5', normal_bold_border)
            worksheet.write(row, 2, '0.6', normal_bold_border)
            worksheet.write(row, 3, '1.1', normal_bold_border)
            worksheet.write(row, 4, '1.2', normal_bold_border)
            worksheet.write(row, 5, '2.1', normal_bold_border)
            worksheet.write(row, 6, '2.2', normal_bold_border)
            worksheet.write(row, 7, '2.3', normal_bold_border)
            worksheet.write(row, 8, '2.4', normal_bold_border)
            worksheet.write(row, 9, '3.1', normal_bold_border)
            worksheet.write(row, 10, '3.2', normal_bold_border)
            worksheet.write(row, 11, '4.1', normal_bold_border)
            worksheet.write(row, 12, '4.2', normal_bold_border)
            worksheet.write(row, 13, '5.1', normal_bold_border)
            worksheet.write(row, 14, '5.2', normal_bold_border)
            worksheet.write(row, 15, '5.3', normal_bold_border)
            worksheet.write(row, 16, '5.4', normal_bold_border)
            worksheet.write(row, 17, '5.5', normal_bold_border)
            worksheet.write(row, 18, '6.1', normal_bold_border)
            worksheet.write(row, 19, '6.2', normal_bold_border)
            worksheet.write(row, 20, '7.1', normal_bold_border)
            worksheet.write(row, 21, '7.2', normal_bold_border)
            worksheet.write(row, 22, '7.3', normal_bold_border)
            worksheet.write(row, 23, '8.1', normal_bold_border)
            worksheet.write(row, 24, '8.2', normal_bold_border)
            worksheet.write(row, 25, '8.3', normal_bold_border)
            worksheet.write(row, 26, '9.1', normal_bold_border)
            worksheet.write(row, 27, '9.2', normal_bold_border)
            worksheet.write(row, 28, '10.1', normal_bold_border)
            row += 1

            worksheet.write(row, 0, 'T/K', normal_bold_border)
            worksheet.write(row, 1, 'ACCOUNT', normal_bold_border)
            worksheet.write(row, 2, 'NPWP', normal_bold_border)
            worksheet.write(row, 3, 'BPJSKES TOTAL', normal_bold_border)
            worksheet.write(row, 4, 'JKK+JKM P', normal_bold_border)
            worksheet.write(row, 5, 'GP', normal_bold_border)
            worksheet.write(row, 6, 'TJ JABATAN', normal_bold_border)
            worksheet.write(row, 7, 'TJ MSKERJA', normal_bold_border)
            worksheet.write(row, 8, 'TJ BERAS', normal_bold_border)
            worksheet.write(row, 9, 'BONUS TETAP', normal_bold_border)
            worksheet.write(row, 10, 'TJ DIDIK', normal_bold_border)
            worksheet.write(row, 11, 'TJ HADIR', normal_bold_border)
            worksheet.write(row, 12, 'TJ TRANS', normal_bold_border)
            worksheet.write(row, 13, 'THR', normal_bold_border)
            worksheet.write(row, 14, 'BONUS KINERJA', normal_bold_border)
            worksheet.write(row, 15, 'LEMBUR', normal_bold_border)
            worksheet.write(row, 16, 'UPD', normal_bold_border)
            worksheet.write(row, 17, 'PROYEK', normal_bold_border)
            worksheet.write(row, 18, 'TAMBAH', normal_bold_border)
            worksheet.write(row, 19, 'KURANG', normal_bold_border)
            worksheet.write(row, 20, 'TOTAL TUNAI 2+3', normal_bold_border)
            worksheet.write(row, 21, 'BRUTTO PPH', normal_bold_border)
            worksheet.write(row, 22, 'PPH', normal_bold_border)
            worksheet.write(row, 23, 'POT JHT K', normal_bold_border)
            worksheet.write(row, 24, 'POT JP K', normal_bold_border)
            worksheet.write(row, 25, 'POT BPJSKES K', normal_bold_border)
            worksheet.write(row, 26, 'TAMBAH', normal_bold_border)
            worksheet.write(row, 27, 'KURANG', normal_bold_border)
            worksheet.write(row, 28, 'TOTAL', normal_bold_border)
            row += 1

            for lines in result[line]:
                if result[line][lines].get('tipe') == '0':
                    tipe_str = 'K'
                elif result[line][lines].get('tipe') == '1':
                    tipe_str = 'T'
                worksheet.write(row, 0,  tipe_str, normal_left)
                worksheet.write(row, 1, result[line][lines].get(
                    'kode_finance'), normal_left)
                # worksheet.write(row, 2, 'npwp', normal_left)
                worksheet.write(row, 2, ' ', normal_left)
                worksheet.write(row, 3, result[line][lines].get(
                    'bpjs_amount') or '0', normal_right)
                worksheet.write(row, 4, jkk_jkm or '0', normal_right)
                worksheet.write(row, 5, result[line][lines].get(
                    'gp_amount'), normal_right)
                worksheet.write(row, 6, result[line][lines].get(
                    'jabt_amount') or '0', normal_right)
                worksheet.write(row, 7, result[line][lines].get(
                    'mskj_amount') or '0', normal_right)
                worksheet.write(row, 8, result[line][lines].get(
                    'bras_amount') or '0', normal_right)
                worksheet.write(row, 9, result[line][lines].get(
                    'bns_amount') or '0', normal_right)
                worksheet.write(row, 10, result[line][lines].get(
                    'tddik_amount') or '0', normal_right)
                worksheet.write(row, 11, result[line][lines].get(
                    'thadir_amount') or '0', normal_right)
                worksheet.write(row, 12, result[line][lines].get(
                    'trns_amount') or '0', normal_right)
                worksheet.write(row, 13, result[line][lines].get(
                    'thr_amount') or '0', normal_right)
                worksheet.write(row, 14, result[line][lines].get(
                    'rapel_amount') or '0', normal_right)
                worksheet.write(row, 15, result[line][lines].get(
                    'lembur_amount') or '0', normal_right)
                worksheet.write(row, 16, result[line][lines].get(
                    'upd_amount') or '0', normal_right)
                worksheet.write(row, 17, result[line][lines].get(
                    'proyek_amount') or '0', normal_right)
                # worksheet.write(row, 18, 'TAMBAH',normal_left)
                worksheet.write(row, 18, ' ', normal_right)
                # worksheet.write(row, 19, 'KURANG',normal_left)
                worksheet.write(row, 19, ' ', normal_right)
                # worksheet.write(row, 20, 'TOTAL TUNAI 2+3',normal_left)
                worksheet.write(row, 20, ' ', normal_right)
                worksheet.write(row, 21, result[line][lines].get(
                    'bruto_amount') or ' ', normal_right)
                # worksheet.write(row, 22, 'PPH',normal_left)
                worksheet.write(row, 22, ' ', normal_right)
                # worksheet.write(row, 23, 'POT JHT K',normal_left)
                worksheet.write(row, 23, ' ', normal_right)
                # worksheet.write(row, 24, 'POT JP K',normal_left)
                worksheet.write(row, 24, ' ', normal_right)
                # worksheet.write(row, 25, 'POT BPJSKES K',normal_left)
                worksheet.write(row, 25, ' ', normal_right)
                # worksheet.write(row, 26, 'TAMBAH',normal_left)
                worksheet.write(row, 26, ' ', normal_right)
                # worksheet.write(row, 27, 'KURANG',normal_left)
                worksheet.write(row, 27, ' ', normal_right)
                worksheet.write(row, 28, result[line][lines].get(
                    'total_amount') or ' ', normal_right)
                row += 1

            row += 1

        worksheet.set_paper(9)
        worksheet.set_margins(0.2, 0.2, 0.5, 0.5)
        worksheet.set_landscape()
        worksheet.center_horizontally()
        workbook.close()
        out = base64.encodestring(bz.getvalue())
        self.write({'state_x': 'get', 'data_x': out, 'name': filename})

        ir_model_data = self.env['ir.model.data']
        bz.close()
        form_res = ir_model_data.get_object_reference(
            'jago_hr_employee', 'kode_finance_wizard_view')
        form_id = form_res and form_res[1] or False
        return {
            'name': ('Download XLS'),
            # 'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'kode.finance.wizard',
            'res_id': self.id,
            'view_id': False,
            'views': [(form_id, 'form')],
            'type': 'ir.actions.act_window',
            'target': 'current'
        }
