"""Report Kode Finance"""
# from odoo.tools.misc import xlsxwriter
from odoo import models, fields, api, _
from datetime import datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from io import BytesIO
import calendar
import base64
import io
from odoo.exceptions import UserError
try:
    from odoo.tools.misc import xlsxwriter
except ImportError:
    import xlsxwriter


class SPTWizard(models.TransientModel):
    _name = "spt.wizard"
    _description = "Export e-SPT"

    @api.model
    def _get_year(self):
        now = int(datetime.now().year)
        return now

    month = fields.Selection([('01', 'Januari'), ('02', 'Februari'), ('03', 'Maret'), ('04', 'April'), ('05', 'Mei'), ('06', 'Juni'),
                              ('07', 'Juli'), ('08', 'Agustus'), ('09', 'September'), ('10', 'Oktober'), ('11', 'November'), ('12', 'Desember')],
                             string='Bulan')
    year = fields.Char(string='Tahun', default=_get_year)
    kode_finance_ids = fields.Many2many('kode.finance', string="Kode Finance")
    state_x = fields.Selection(
        (('choose', 'choose'), ('get', 'get')), default='choose')
    data_x = fields.Binary('File', readonly=True)
    name = fields.Char('Filename', readonly=True)

    @api.onchange('month', 'year')
    def onchange_month(self):
        if self.month and self.year:
            lastday = calendar.monthrange(int(self.year), int(self.month))[1]
            self.date_from = '%s-%s-01' % (int(self.year), self.month)
            self.date_to = '%s-%s-%s' % (int(self.year), self.month, lastday)

    def convert_format_date(self, date):
        date = datetime.strptime(date, DF)
        date = date.strftime(DF)
        return date

    def month_name(self, month_number):
        return calendar.month_name[month_number]

    def month_number(self, month_number):
        return calendar.month_name

    def generate_excel(self):
        self.ensure_one()
        bz = BytesIO()
        workbook = xlsxwriter.Workbook(bz)
        if self.year and self.month:
            filename = 'Export e-SPT ('+str(self.year)+').xls'
        else:
            filename = 'Export e-SPT.xls'

        if self.kode_finance_ids:
            kd_ids = self.kode_finance_ids
        else:
            kd_ids = self.env['kode.finance'].search([])

        query = """
            select
            kf.id,
            he.id,
            he.nip as nip,
            he.name as emp_name,
            he.npwp as emp_npwp,
    		hd.id,
    		hd.name as department,
            hp.id,
            kf.id as kode_id,
            kf.name as kode_name,
            kf.finance_code as kode_finance,
            kf.finance_type as tipe,
            count(he.id) as emp_count,
            hsr.id as rule_id,
            hsr.code as rule_code,
            hp.date_to as hp_date,
            sum(hpl.total) as slip_line_total
            from kode_finance kf
            join hr_employee he on kf.id =he.lokasi_kerja_id
            join hr_payslip hp on hp.employee_id=he.id
            left join hr_department hd on hd.id = he.department_id
            left join hr_payslip_line hpl on hpl.slip_id = hp.id
            left join hr_salary_rule hsr on hsr.id = hpl.salary_rule_id
        """

        params = ()
        if not self.kode_finance_ids:
            raise UserError(
                _('Pilih Kode Finance !'))
        else:
            query += """
            where kf.id in %s
            """
            params = (tuple(self.kode_finance_ids.ids),)
            if self.month:
                month = self.month
                query += " AND extract(month from hp.date_to) = %s"
                params += (month,)
            if self.year:
                year = self.year
                query += " AND extract(year from hp.date_to) = %s"
                params += (year,)

        query += """
            group by kf.id, kf.finance_code, hsr.id, hsr.code, he.id, hp.id,
            hd.id
            order by hp.date_to asc
        """

        self.env.cr.execute(query, params)
        datas = self.env.cr.dictfetchall()
        if not datas:
            raise UserError(_('No Data'))

        title_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 16,
             'valign': 'vcenter', 'align': 'center', 'text_wrap': True,
             'font_color': 'blue'})
        #################################################################################
        normal_style = workbook.add_format(
            {'valign': 'vcenter', 'text_wrap': True,
             'font_size': 11, 'font_name': 'Arial'})
        #################################################################################
        normal_left = workbook.add_format(
            {'valign': 'bottom', 'align': 'left'})
        normal_left.set_text_wrap()
        normal_left.set_font_name('Arial')
        normal_left.set_font_size('10')
        normal_left.set_border()
        #################################################################################
        normal_right = workbook.add_format(
            {'valign': 'bottom', 'align': 'right'})
        normal_right.set_text_wrap()
        normal_right.set_font_name('Arial')
        normal_right.set_font_size('10')
        normal_right.set_border()
        #################################################################################
        normal_left1 = workbook.add_format(
            {'valign': 'bottom', 'align': 'left'})
        normal_left1.set_text_wrap()
        normal_left1.set_font_name('Arial')
        normal_left1.set_font_size('10')
        #################################################################################
        normal_left_bold = workbook.add_format(
            {'bold': 1, 'valign': 'bottom', 'align': 'left'})
        normal_left_bold.set_text_wrap()
        normal_left_bold.set_font_name('Arial')
        normal_left_bold.set_font_size('10')
        #################################################################################
        normal_bold_border = workbook.add_format(
            {'bold': 0, 'valign': 'vcenter', 'align': 'center'})
        normal_bold_border.set_text_wrap()
        normal_bold_border.set_font_name('Arial')
        normal_bold_border.set_font_size('10')
        normal_bold_border.set_border()

        worksheet = workbook.add_worksheet("report")
        worksheet.set_column('A:A', 13)
        worksheet.set_column('B:B', 9)
        worksheet.set_column('C:C', 13)
        worksheet.set_column('D:D', 17)
        worksheet.set_column('E:AB', 13)
        worksheet.set_column('AC:AC', 20)

        row = 1
        result = {}
        list_kode = []
        list_month = []
        for dt in datas:
            kode = dt.get('kode_id')
            month_int = fields.Date.from_string(
                dt.get('hp_date')).strftime('%m')
            year = fields.Date.from_string(dt.get('hp_date')).strftime('%Y')
            list_kode.append(kode)
            list_month.append(month_int)
            rule = dt.get('rule_code')
            jkk_jkm = 0
            gp_a = 0
            transfer = 0
            total = 0
            no = 1

            key = "%s" % (month_int)
            if key not in result:
                result[key] = { '%s' % (kode) : {
                    # result [kode] = {
                    'date': dt.get('hp_date'),
                    'nip': dt.get('nip'),
                    'emp_name': dt.get('emp_name'),
                    'department': dt.get('department'),
                    'tipe': dt.get('tipe'),
                    'kode_finance': dt.get('kode_finance'),
                    'emp_npwp': dt.get('emp_npwp'),
                    'name': dt.get('kode_name'),
                    'month': month_int,
                    'year': year,
                    'nilai': dt.get('slip_line_total'),
                    'rule_code': rule,
                    # 'emp_count': dt.get('emp_count'),
                    'bruto_amount': rule == 'GROSS_PAJAK' and dt.get('slip_line_total') or 0,
                    'pph_amount': rule == 'PPH21' and dt.get('slip_line_total') or 0,
                }}
            else:
                if '%s' % (kode) not in result[key].keys():
                    result[key].update({ '%s' % (kode) : {
                        'date': dt.get('hp_date'),
                        'nip': dt.get('nip'),
                        'emp_name': dt.get('emp_name'),
                        'department': dt.get('department'),
                        'tipe': dt.get('tipe'),
                        'kode_finance': dt.get('kode_finance'),
                        'emp_npwp': dt.get('emp_npwp'),
                        'year': year,
                        'nilai': dt.get('slip_line_total'),
                        'rule_code': rule,
                        'bruto_amount': dt.get('slip_line_total') if rule == 'GROSS_PAJAK' else 0,
                        'pph_amount': dt.get('slip_line_total') if rule == 'PPH21' else 0,
                        }})
                else:
                    vals = result[key]['%s' % (kode)]
                    vals['bruto_amount'] += rule == 'GROSS_PAJAK' and dt.get(
                        'slip_line_total') or 0
                    vals['pph_amount'] += rule == 'PPH21' and dt.get(
                        'slip_line_total') or 0

        for line in result:
            worksheet.set_row(row, 35)
            if line == '01':
                month_str = 'Januari'
            elif line == '02':
                month_str = 'Februari'
            elif line == '03':
                month_str = 'Maret'
            elif line == '04':
                month_str = 'April'
            elif line == '05':
                month_str = 'Mei'
            elif line == '06':
                month_str = 'Juni'
            elif line == '07':
                month_str = 'Juli'
            elif line == '08':
                month_str = 'Agustus'
            elif line == '09':
                month_str = 'September'
            elif line == '10':
                month_str = 'Oktober'
            elif line == '11':
                month_str = 'November'
            elif line == '12':
                month_str = 'Desember'

            worksheet.write(row, 0, 'payslip_date', normal_bold_border)
            worksheet.write(row, 1, 'no', normal_bold_border)
            worksheet.write(row, 2, 'hr.employee.nip', normal_bold_border)
            worksheet.write(row, 3, 'employee.name', normal_bold_border)
            worksheet.write(row, 4, 'hr.employee.departement_id', normal_bold_border)
            worksheet.write(row, 5, 'kode.finance.finance_type', normal_bold_border)
            worksheet.write(row, 6, 'kode.finance.Finance_code', normal_bold_border)
            worksheet.write(row, 7, 'hr.employee.npwp', normal_bold_border)
            worksheet.write(row, 8, 'payslip.line.GROSS_PAJAK', normal_bold_border)
            worksheet.write(row, 9, 'payslip.line.PPH21', normal_bold_border)
            row += 1

            for lines in result[line]:
                if result[line][lines].get('tipe') == '0':
                    tipe_str = 'K'
                elif result[line][lines].get('tipe') == '1':
                    tipe_str = 'T'
                worksheet.write(row, 0, month_str + ' - '+ year , normal_left)
                worksheet.write(row, 1, no, normal_left)
                worksheet.write(row, 2, result[line][lines].get(
                    'nip'), normal_left)
                worksheet.write(row, 3, result[line][lines].get(
                    'emp_name'), normal_left)
                worksheet.write(row, 4, result[line][lines].get(
                    'department'), normal_left)
                worksheet.write(row, 5, tipe_str , normal_left)
                worksheet.write(row, 6, result[line][lines].get(
                    'kode_finance'), normal_left)
                worksheet.write(row, 7, result[line][lines].get(
                    'emp_npwp'), normal_left)
                worksheet.write(row, 8, result[line][lines].get(
                    'bruto_amount') or ' ', normal_left)
                worksheet.write(row, 9, abs(result[line][lines].get(
                    'pph_amount')) or ' ', normal_left)
                row += 1
                no = no + 1
            row += 1

        worksheet.set_paper(9)
        worksheet.set_margins(0.2, 0.2, 0.5, 0.5)
        worksheet.set_landscape()
        worksheet.center_horizontally()
        workbook.close()
        out = base64.encodestring(bz.getvalue())
        self.write({'state_x': 'get', 'data_x': out, 'name': filename})

        ir_model_data = self.env['ir.model.data']
        bz.close()
        form_res = ir_model_data.get_object_reference(
            'jago_hr_employee', 'spt_wizard_view')
        form_id = form_res and form_res[1] or False
        return {
            'name': ('Download XLS'),
            # 'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'spt.wizard',
            'res_id': self.id,
            'view_id': False,
            'views': [(form_id, 'form')],
            'type': 'ir.actions.act_window',
            'target': 'current'
        }
