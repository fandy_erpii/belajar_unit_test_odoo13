import base64
import logging

from odoo import api, fields, models
from odoo import tools, _
from datetime import datetime, timedelta
from odoo.exceptions import ValidationError, AccessError, UserError
from odoo.modules.module import get_module_resource
from dateutil.relativedelta import relativedelta

class HrEmployee(models.Model):
    _inherit = 'hr.employee'
    nip = fields.Char(string='Nomor Induk Pegawai')
    nomor_kk = fields.Char(string='Nomor Kartu Keluarga')
    npwp =  fields.Char(string='NPWP')
    no_rekening = fields.Char(string='No Rekening')
    nomor_bpjs = fields.Char(string='No BPJS Kesehatan')
    ikut_bpjs = fields.Boolean("Ikut BPJS")
    reimburs_bpjs = fields.Boolean(string='Reimburs BPJS')
    nomor_bpjs_ketenagakerjaan = fields.Char(string='No BPJS Ketenagakerjaan')
    lokasi_kerja_id = fields.Many2one('kode.finance', string='Lokasi Kerja')
    kode_finance = fields.Char(string='Kode Finance')
    usia  = fields.Char(string="Usia")
    pensiun  = fields.Boolean("Jaminan Pensiun")
    perkawinan = fields.Selection([
        ('single', 'Tidak Kawin'),
        ('married', 'Kawin'),
    ], string='Status Perkawinan', groups="hr.group_hr_user", default='single')
    pendidikan = fields.Selection([
        ('s3', 'Doktor (S3)'),
        ('s2', 'Magister (S2)'),
        ('s1', 'Sarjana (S1)'),
        ('d3', 'Diploma (D3)'),
        ('sma_smk', 'Sekolah Menengah Atas (SMA / SMK)'),
        ('smp', 'Sekolah Menengah Pertama (SMP)'),
        ('sd', 'Sekolah Dasar (SD)'),
    ], 'Certificate Level', default='s1', groups="hr.group_hr_user")
    tgl_masuk_kerja = fields.Datetime(string = "Tanggal Masuk Kerja")
    hari_ini = fields.Datetime(string = "Hari ini", default = fields.Datetime.now(),readonly=True)
    masa_kerja = fields.Char(string = "Masa Kerja",invisible=1)
    str_tahun_kerja = fields.Char(string = "Tahun Kerja")
    int_tahun_kerja = fields.Integer(string = "Tahun Kerja")
    training_employee = fields.One2many('hr.training', 'employee_obj', string='Training History')
    punishment_history = fields.One2many('hr.training', 'employee_punishment', string='Punishment/Reward History')
    alamat = fields.Text(string="Alamat")

    @api.onchange('lokasi_kerja_id')
    def _onchange_lokasi_kerja_id(self):
        self.kode_finance = self.lokasi_kerja_id.finance_code

    @api.onchange('tgl_masuk_kerja')
    def calculate_date(self):
        for rec in self:
            if rec.tgl_masuk_kerja:
                dt = rec.tgl_masuk_kerja
                d2 = datetime.now()
                rd = relativedelta(d2, dt)

                rec.masa_kerja = str(rd.years)
                if rec.masa_kerja == '0':
                    rec.str_tahun_kerja = str(rd.months) + ' Bulan '
                    rec.int_tahun_kerja = int(rd.months)
                else:
                    rec.str_tahun_kerja = '%s Tahun, %s Bulan' %(rd.years,rd.months)      #str(rd.years) + ' Tahun ' + int(rd.years)
                    rec.int_tahun_kerja = int(rd.years)
    number = fields.Char(string='Number')
    kontak = fields.Char(string='Contact')
    employee_obj_contact = fields.Many2one('hr.employee', invisible=1)

    def _birhday_reminder(self):
        for employee in self.search([]):
            if employee.birthday:            
                tahun = fields.Datetime.today().year
                ulangtahun = employee.birthday.replace(year=tahun)
                umur = relativedelta(ulangtahun,employee.birthday).years         
                check_birthday_employee = employee.env['calendar.event'].sudo().search(['&',('start_date','=',ulangtahun),('description', 'ilike', employee.nip)])              
                if check_birthday_employee:
                    pass
                if not check_birthday_employee:                             
                    employee.env['calendar.event'].create({
                        'name': '%s - %s BIRTHDAY %s' % (employee.nip,employee.name,umur),
                        'description': employee.nip,
                        'partner_ids': False,
                        'user_id' : False,
                        'partner_id' : False,                        
                        'allday': True,
                        'duration': False,
                        'start_date': ulangtahun,
                        'stop_date': ulangtahun,
                        'start': ulangtahun,
                        'stop':ulangtahun,
                    })

class HrEmployeeTraining(models.Model):
    """This class is to add training history and punishment history in private information hr.employee"""

    _name = 'hr.training'

    nama_training = fields.Char(string='Nama Training')
    keterangan_training = fields.Char(string='Keterangan')
    tanggal_training = fields.Date(string='Tanggal Training')
    employee_obj = fields.Many2one('hr.employee', invisible=1)

    nama_punishment = fields.Char(string='Nama Punishment/Reward')
    tanggal_punishment = fields.Date(string='Tanggal Punishment/Reward')
    keterangan_punishment = fields.Char(string='Keterangan')
    employee_punishment = fields.Many2one('hr.employee', invisible=1)



class HrContract(models.Model):
     _inherit = 'hr.contract'
     currency_id = fields.Many2one(string="Currency", related='company_id.currency_id', readonly=True)
     t_jabatan = fields.Monetary(string='Tunjangan Jabatan')
     bonus_tetap = fields.Monetary(string='Bonus Tetap')


class KodeFinance(models.Model):
    _name = 'kode.finance'
    _inherit = ['mail.thread']

    TYPE_VOUCHER = [
        ('0', 'Kontrak'),
        ('1', 'Tetap'),
    ]

    currency_id = fields.Many2one('res.currency', 'Currency', required=True,
        default=lambda self: self.env.company.currency_id.id)
    name = fields.Char('Lokasi Kerja', required=True, track_visibility='onchange')
    finance_code = fields.Char(string='Kode Finance',required=True, track_visibility='onchange')
    finance_type = fields.Selection(TYPE_VOUCHER,string='Tipe',default='0', required=True, track_visibility='onchange')
    gaji_pokok = fields.Monetary(string='UMR/UMK', required=True,
            track_visibility='onchange')

class HrPayslipInherit(models.Model):
    _inherit = 'hr.payslip'

    lokasi_id = fields.Many2one('kode.finance', string='Lokasi Kerja', store=True)
    financecode = fields.Char(string='Kode Finance')

    @api.onchange('employee_id')
    def _onchange_employee_id(self):
        self.lokasi_id = self.employee_id.lokasi_kerja_id
        self.financecode = self.employee_id.kode_finance

            
        if self.employee_id == self.contract_id.employee_id: # mencari apakah ada contract yang dimiliki employee
            dic_parts = {} # penampung line description salary
            for a in self.input_line_ids:
                a.unlink() # menghapus dulu semua line yg sudah terbuat

            terms_obj = self.env['hr.payroll.structure']
            terms = []
            termsids = terms_obj.search([('id','=',self.struct_id.id)]) #mencari payroll structure yang sesuai dengan salary
 
            for rec in termsids.input_line_type_ids:
                values = {}
                values['name'] = rec.name
                values['input_type_id'] = rec.id
                res = terms.append((0, 0, values)) # menambahkan ke penampung line ids
            
            self.input_line_ids = terms # nilai line adalah penapung line
        else:
            print("...........", self.employee_id.name)
            for a in self.input_line_ids:
                a.unlink()

    def fungsi_test(self):
        print("........Menghubungkan UNit Test/.........")

       
       
class HrContractInherit(models.Model):
    _inherit = 'hr.contract'

    area_id = fields.Many2one('kode.finance', string='Lokasi Kerja', store=True)
    financekode = fields.Char(string='Kode Finance')

    @api.onchange('employee_id')
    def _onchange_employee_id(self):
        self.area_id = self.employee_id.lokasi_kerja_id
        self.financekode = self.employee_id.kode_finance
        self.department_id = self.employee_id.department_id.id
        self.job_id = self.employee_id.job_id
