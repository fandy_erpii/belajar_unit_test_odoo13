odoo.define("jago_hr_employee.CetakSlipGaji", function (require) {
  "use strict";

  var ListController = require("web.ListController");

  ListController.include({
    renderButtons: function ($node) {
      this._super.apply(this, arguments);
      if (this.$buttons) {
        this.$buttons
          .find(".o_button_to_call_wizard")
          .click(this.proxy("action_def"));
      }
    },

    //--------------------------------------------------------------------------
    // Define Handler for new Custom Button
    //--------------------------------------------------------------------------

    /**
     * @private
     * @param {MouseEvent} event
     */
    action_def: function (e) {
      var self = this;
      self.do_action({
        name: "Cetak Slip Gaji",
        type: "ir.actions.act_window",
        res_model: "cetak.slip.gaji.report.wizard",
        view_mode: "form",
        view_type: "form",
        views: [[false, "form"]],
        target: "new",
      });
    },
  });
});
