# -*- coding: utf-8 -*-
{
    'name': "Jago HR",

    'summary': """
       Kebutuhan Custom Employee PT Jago Group""",

    'description': """
        Add NIP
        Add No.KK
        Add NPWP
        Add No.BPJS Kesehatan
        Add No.BPJS Ketenagakerjaan
        Add Kode Finance
        Add Masa Kerja
        Add Training History, Punishment/Reward History
        Add Kode Voucher
    """,

    'author': "PT. ERP Indonesia",
    'website': "https://erpindonesia.co.id/",

    'category': 'Uncategorized',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': [
        'base',
        'hr',
        'hr_contract',
        'hr_gamification',
        'hr_payroll',
    ],

    # always loaded
    'data': [
        'security/group.xml',
        'security/ir.model.access.csv',
        'wizard/voucher_wizard_view.xml',
        'views/kode_finance_views.xml',
        'views/hr_employee_views.xml',
        'views/hr_contract_views.xml',
        'views/assets_backend.xml',
        'wizard/cetak_slip_gaji.xml',
        'wizard/cetak_spt_view.xml',
        'report/report_format.xml',
        'report/report_cetak_slip_gaji.xml'
    ],
    'qweb':  ['static/src/xml/qweb.xml',],
}
