# # -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from unittest.mock import patch
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.fields import Date
from odoo.exceptions import UserError
from odoo.tests.common import TransactionCase, new_test_user
from odoo.addons.test_mail.tests.common import mail_new_test_user


class TestRuleParameter(TransactionCase):

    def setUp(self):
        super(TestRuleParameter, self).setUp()

        self.res_users_hr_officer = mail_new_test_user(self.env, login='hro', groups='base.group_user,hr.group_hr_user', name='HR Officer', email='hro@example.com')

        emp0 = self.env['hr.employee'].create({
            'user_id': self.res_users_hr_officer.id,
        })
        self.department = self.env['hr.department'].create({
            'name': 'Test Department',
            'member_ids': [(4, emp0.id)],
        })

        print("...>", emp0)


        # I create a new Rules Paramater
        self.rule_parameter = self.env['hr.rule.parameter'].create({
            'name': 'Test Parameter',
            'code': 'test_param',
        })

        # I create a new Rules
        values = []
        for year in [2016, 2017, 2018, 2020]:
            values.append({
                'rule_parameter_id': self.rule_parameter.id,
                'parameter_value': str(year),
                'date_from': date(year, 1, 1)
            })
        rules = self.env['hr.rule.parameter.value'].create(values)


        # I create a new employee "Richard"
        self.richard_emp = self.env['hr.employee'].create({
            'name': 'Richard',
            'gender': 'male',
            'birthday': '1984-05-01',
            'country_id': self.ref('base.be'),
            'department_id': self.department.id
        })

        # I create a new Employee
        self.jules_emp = self.env['hr.employee'].create({
            'name': 'Jules',
            'gender': 'male',
            'birthday': '1984-05-01',
            'country_id': self.ref('base.be'),
            'department_id': self.department.id
        })

        print("-->",self.richard_emp.name)

        self.structure_type = self.env['hr.payroll.structure.type'].create({
            'name': 'Test - Developer',
        })

        # I create a contract for "Richard"
        self.contratct1 = self.env['hr.contract'].create({
            'date_end': Date.today() + relativedelta(years=2),
            'date_start': Date.to_date('2018-01-01'),
            'name': 'Contract for Richard',
            'wage': 5000.0,
            'employee_id': self.richard_emp.id,
            'structure_type_id': self.structure_type.id,
        })

        print("---------satu-    empat------", self.contratct1)

        # activate Richard's contract
        self.richard_emp.contract_ids[0].state = 'open'

        # I create an employee Payslip
        richard_payslip = self.env['hr.payslip'].create({
            'name': 'Payslip of Richard',
            'employee_id': self.richard_emp.id
        })

        richard_payslip.fungsi_test()

        payslip_input = self.env['hr.payslip.input'].search([('payslip_id', '=', richard_payslip.id)])
        # I assign the amount to Input data
        payslip_input.write({'amount': 5.0})

        # I verify the payslip is in draft state
        self.assertEqual(richard_payslip.state, 'draft', 'State not changed!')
        print(".......ok.........")

        context = {
            "lang": "en_US", "tz": False, "active_model": "ir.ui.menu",
            "department_id": False, "section_id": False,
        }
        # I click on 'Compute Sheet' button on payslip
        richard_payslip.with_context(context).compute_sheet()

        # Then I click on the 'Confirm' button on payslip
        richard_payslip.action_payslip_done()

        print(".......Done.........")


    @patch.object(Date, 'today', lambda: date(2019, 10, 10))
    def test_get_last_version(self):
        value = self.env['hr.rule.parameter']._get_parameter_from_code('test_param')
        self.assertEqual(value, 2018, "It should get last valid value")
        print(">>>>>>>>>Test 1x <<<<<<<<<<<")


